import axios from 'axios'

export default {
    state: {
        token: localStorage.getItem('token') || null
    },
    actions: {
        async signup(context, user) {
            return new Promise((resolve, reject) => {
                axios.post(`${this.state.api}account-signup`, {
                    email: user.email,
                    password: user.password
                })
                    .then((response) => {
                        localStorage.setItem('token', response.data.data.token)
                        context.commit('retrieveToken', response.data.data.token)
                        resolve(response.data)
                    })
                    .catch((error) => { reject(error) })
            })
        },
        async signin(context, user) {
            return new Promise((resolve, reject) => {
                axios.post(`${this.state.api}account-signin`, {
                    email: user.email,
                    password: user.password,
                })
                    .then(response => {
                        const token = response.data.data.token
                        localStorage.setItem('token', token)
                        context.commit('retrieveToken', token)
                        resolve(response)
                    })
                    .catch(error => { reject(error) })
            })
        },
        async signout(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post(`${this.state.api}account-signout`)
                        .then(response => {
                            localStorage.removeItem('token')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => { reject(error) })
                })
            }
        },
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        }
    },
    getters: {
        getToken(state) {
            return state.token
        },
        loggedIn(state) {
            return state.token !== null
        },
    }
}
