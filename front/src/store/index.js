import { createStore } from 'vuex'
import notebook from './note'
import account from './account'

export const store = createStore({
  state: {
    api: `http://notebook-awpi/api/`,
  },
  modules: {
    notebook, account
  }
})
