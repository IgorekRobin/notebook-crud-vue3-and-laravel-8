import axios from 'axios'

export default {
	actions: {
		async getNotes(context) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.getters.getToken
			return new Promise((resolve, reject) => {
				axios.get(`${this.state.api}note`)
				.then(response => { resolve(response.data)})
				.catch(error => { reject(error) })
			})
		},
		async getNote(context, id) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.getters.getToken
			return new Promise((resolve, reject) => {
				axios.get(`${this.state.api}note/${id}`)
				.then(response => {resolve(response.data)})
				.catch(error => { reject(error) })
			})
		},
		async createNote(context, note) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.getters.getToken
			return new Promise((resolve, reject) => {
				axios.post(`${this.state.api}note`, {
					title: note.title,
					description: note.description
				})
				.then(response => {resolve(response.data)})
				.catch(error => { reject(error) })
			})
		},
		async updateNote(context, note) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.getters.getToken
			return new Promise((resolve, reject) => {
				axios.patch(`${this.state.api}note/${note.id}`, {
					title: note.title,
					description: note.description,
				})
				.then(response => {resolve(response.data)})
				.catch(error => { reject(error) })
			})
		},
		async deleteNote(context, id) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.getters.getToken
			return new Promise((resolve, reject) => {
				axios.delete(`${this.state.api}note/${id}`)
				.then(response => {resolve(response.data)})
				.catch(error => { reject(error) })
			})
		},
	}
}