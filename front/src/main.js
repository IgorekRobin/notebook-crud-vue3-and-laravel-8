import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css'

import messagePlugin from '@/utils/message.plugin'

const app = createApp(App)

app.use(store)
app.use(router)
app.use(ElementPlus)
app.use(messagePlugin)

app.mount('#app')
