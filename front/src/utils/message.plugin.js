export default {
    install(app) {
      app.config.globalProperties.$messageSuccess = function(title, message) {
        this.$notify({
          title,
          message,
          type: 'success',
          position: 'bottom-right'
        });
      }
      app.config.globalProperties.$messageError = function(title, message) {
        this.$notify({
          title,
          message,
          type: 'error',
          position: 'bottom-right'
        });
      }
    }
  }