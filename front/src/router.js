import {createWebHistory, createRouter} from "vue-router";
import { store } from './store/index'
const routes = [
    {
        path: '/',
        name: 'note',
        meta: { auth: true, layout: 'main' },
        component: () => import('./views/Note.vue')
    },
    {
        path: '/note/create',
        name: 'note-create',
        meta: { auth: true, layout: 'main'},
        component: () => import('./views/NoteCreate.vue')
    },
    {
        path: '/note/edit/:id',
        name: 'note-edit',
        meta: { auth: true, layout: 'main' },
        component: () => import('./views/NoteEdit.vue')
    },
    {
        path: '/account/signup',
        name: 'account-signup',
        meta: { auth: false, layout: 'auth' },
        component: () => import('./views/AccountSignup.vue')
    },
    {
        path: '/account/signin',
        name: 'account-signin',
        meta: { auth: false, layout: 'auth' },
        component: () => import('./views/AccountSignin.vue')
    },

    {
        path: '/account/signout',
        name: 'account-signout',
        meta: { auth: true, layout: 'main' },
        component: () => import('./views/AccountSignout.vue')
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    if (
        'auth' in to.meta &&
        to.meta.auth &&
        !store.getters.loggedIn
    ) {
        next({ name: 'account-signup' });

    } else if (
        'auth' in to.meta &&
        !to.meta.auth &&
        store.getters.loggedIn
    ) {
        next({ name: 'note' });

    } else {
        next();
    }
});

export default router;