<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\NoteRequest;
use App\Models\Note;
use App\Models\User;

class NoteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $user = User::find(auth('api')->user()->id);

    $notes = $user->notes;
    $notes= User::find(auth('api')->user()->id)->notes()->orderBy('id', 'desc')->get();
    return $this->sendResponse($notes, 'success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteRequest $request)
    {
        $user = User::find(auth('api')->user()->id);

        $note = new Note;
        $note->title = $request->title;
        $note->description = $request->description;

        $user = $user->notes()->save($note);

         if($user) {
            return $this->sendResponse($user['title'], 'Запись успешно создана!');
        } else {
            return $this->sendError('error', 'error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $note = Note::select("id", "title", "description")->where('id', $id)->where('user_id', auth('api')->user()->id)->first();
            return $this->sendResponse($note, 'success');
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->sendError('error', 'error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NoteRequest $request, $id)
    {
       $validated = $request->validated();

       $update = Note::where('id', $id)->where('user_id', auth('api')->user()->id)->update([
        'title' => $request->title,
        'description' => $request->description,
    ]);
       if($update) {
           return $this->sendResponse('Запись успешно обновлена!', 'success');
        } else {
            return $this->sendError('error', 'error');
        }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Note::destroy($id);
        return $this->sendResponse('Запись успешно удалена!', 'Запись успешно удалена!');
    }
}
