<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\NoteController;
use App\Http\Controllers\Api\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('account-signup', [RegisterController::class, 'register']);
Route::post('account-signin', [RegisterController::class, 'login']);
Route::post('account-signout', [RegisterController::class, 'logout']);
// Route::post('logout','UserController@logoutApi');

Route::middleware('auth:api')->group( function () {
    Route::resource('note', NoteController::class, ['only' => [
        'index', 'store', 'show', 'update', 'destroy'
    ]]);
});
